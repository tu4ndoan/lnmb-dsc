﻿using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ServerComps.Model;
using System.Net;

namespace ServerComps.DedicatedServer
{
    class DSConnection
    {
        public Socket socket;
        public Thread receiveDataThread;
        public bool bWantStopReceivingData;
        public bool bIsAvailable;
        public bool bIsAccepted;
        public Server server;

        // DS IP
        public string dsIP;

        // DS listening port for gameplay
        public int dsPort;

        // Current room
        public RoomObject currentRoom;

        // Keep connection timer
        public System.Timers.Timer KeepConnectionTimer;

        // Contructor
        public DSConnection(Socket inSocket, Server inServer)
        {
            SetAccepted(false);
            SetAvailable(true);

            bWantStopReceivingData = false;
            socket = inSocket;
            server = inServer;

            dsIP = (socket.RemoteEndPoint as IPEndPoint).Address.ToString();

            StartListeningToDedicatedServer();
        }

        public bool AreYouAvailable()
        {
            return bIsAvailable && bIsAccepted;
        }

        public void SetAccepted(bool Value)
        {
            bIsAccepted = Value;
        }

        public void SetAvailable(bool Value)
        {
            bIsAvailable = Value;
        }        

        public void SetCurrentRoom(RoomObject inRoom)
        {
            SetAvailable(false);
            currentRoom = inRoom;
            currentRoom.SetRoomState(E_RoomState.WaitingForDedicatedServerLoadingMap);
        }

        public void KeepConnection()
        {
            KeepConnectionTimer = new System.Timers.Timer(180000);
            KeepConnectionTimer.Elapsed += OnKeepConnectionEvent;
            KeepConnectionTimer.AutoReset = true;
            KeepConnectionTimer.Enabled = true;
        }

        private void OnKeepConnectionEvent(Object source, ElapsedEventArgs e)
        {
            // Send nothing to keep the connection
            Packet packet = new Packet(-1, "");
            packet.Send(socket, false);
        }

        public void StartListeningToDedicatedServer()
        {
            receiveDataThread = new Thread(server.dsController.ReceiveDataFromDedicatedServer);
            receiveDataThread.Start(this);
        }

        public void HandleDisconnected()
        {
            if (socket != null)
            {
                // Close connection
                socket.Close();
                socket.Dispose();              
            }

            if (KeepConnectionTimer != null)
            {
                KeepConnectionTimer.Stop();
                KeepConnectionTimer.Dispose();
            }

            // Kill the thread lastly
            bWantStopReceivingData = true;
            receiveDataThread.Abort();
        }
    }
}
