﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerComps.DedicatedServer
{
    class DSListener
    {
        public Socket listeningToDSsSocket;
        ConnectionInfo listeningToDSsConnection;
        public Thread listeningToDSsThread;
        public bool bWantStopListening;
        public Server server;

        public DSListener(Server inServer)
        {
            bWantStopListening = false;

            server = inServer;
            listeningToDSsConnection = new ConnectionInfo(server.serverConfig.thisServerIP, server.serverConfig.listenDSPort);
            listeningToDSsSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listeningToDSsSocket.Bind(listeningToDSsConnection.IP_EndPoint);

            StartListeningToDSs();
        }

        public void StartListeningToDSs()
        {
            listeningToDSsThread = new Thread(ListenToDedicatedServersThread);
            listeningToDSsThread.Start();

            Server.LOG("Ready for Dedicated Servers to connect", E_LogCategory.Successful);
        }

        public void StopListening()
        {
            bWantStopListening = false;
        }

        public void ListenToDedicatedServersThread()
        {
            while (bWantStopListening == false)
            {
                listeningToDSsSocket.Listen(1);
                DSConnection dsc = new DSConnection(listeningToDSsSocket.Accept(), server);           
                server.dsController.AddNewDSConnection(dsc);
                Server.LOG("<Dedicated Server> connected", E_LogCategory.Log);
            }
        }
    }
}
