﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace ServerComps.MappingObject.MasterServer
{
    [DataContract]
    class MS_Action_1
    {
        [DataMember(Name = "roomID")]
        public string roomID = "";

        public MS_Action_1()
        {

        }

        public static void Convert(Packet packet, out MS_Action_1 targetInstance)
        {
            targetInstance = new MS_Action_1();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as MS_Action_1;
        }
    }
}
