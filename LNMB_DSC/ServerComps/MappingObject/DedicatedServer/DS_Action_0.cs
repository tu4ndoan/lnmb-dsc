﻿using Utilities;
using Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ServerComps.MappingObject.DedicatedServer
{
    [DataContract]
    class DS_Action_0 : MasterJSON
    {
        [DataMember(Name = "secret")]
        public string secretKey = "";

        [DataMember(Name = "port")]
        public int listeningPort;

        public DS_Action_0()
        {

        }

        public static void Convert(Packet packet, out DS_Action_0 targetInstance)
        {
            targetInstance = new DS_Action_0();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(packet.data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as DS_Action_0;
        }
    }
}
