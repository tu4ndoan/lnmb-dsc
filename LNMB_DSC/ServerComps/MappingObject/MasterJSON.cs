﻿using Utilities;
using Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ServerComps.MappingObject
{
    [DataContract]
    public abstract class MasterJSON
    {
        public MasterJSON()
        {

        }

        public static string GetJsonString<T>(T Object)
        {
            return DataContractJsonSerializerHelper.GetJson(Object);
        }
    }
}
