﻿using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServerComps.MappingObject.MasterServer;
using ServerComps.Model;
using ServerComps.DedicatedServer;

namespace ServerComps.Controller
{
    public enum E_DSCToMasterServerActions
    {
        HereIsMyCurrentState = 0,
        DedicatedServerIsReadyToPlay = 1
    }

    class MasterServerController
    {
        public Socket masterServerSocket;
        public ConnectionInfo masterServerInfo;
        public Thread listeningToMasterServerThread;
        public bool bWantStopReceivingDataFromMSThread;
        public Server server;

        public MasterServerController(Server inServer)
        {
            server = inServer;
            masterServerInfo = new ConnectionInfo(server.serverConfig.masterServerIP, server.serverConfig.masterServerPort);
            bWantStopReceivingDataFromMSThread = false;
            masterServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start()
        {
            if(ConnectToMasterServer())
            {
                StartListeningToMasterServer();
            }
        }

        public bool ConnectToMasterServer()
        {
            try
            {
                masterServerSocket.Connect(masterServerInfo.IP_EndPoint);
                Server.LOG("Connect to Master Server successfully", E_LogCategory.Successful);
                return true;
            }
            catch
            {
                Server.LOG("Connect to Master Server faild", E_LogCategory.Error);
                return false;
            }
        }

        public void HandleMasterServerDisconnected()
        {
            bWantStopReceivingDataFromMSThread = true;
            listeningToMasterServerThread.Abort();
        }

        public void StartListeningToMasterServer()
        {
            listeningToMasterServerThread = new Thread(ReceiveDataFromMasterServer);
            listeningToMasterServerThread.Start();
        }

        public void ReceiveDataFromMasterServer()
        {
            byte[] Buffer;
            int readBytes;

            while (bWantStopReceivingDataFromMSThread == false)
            {
                try
                {
                    Buffer = new byte[masterServerSocket.SendBufferSize];
                    readBytes = masterServerSocket.Receive(Buffer);
                    Array.Resize(ref Buffer, readBytes);

                    if (readBytes > 0)
                    {
                        Packet pk = new Packet(Buffer);
                        DataProcessorForMasterServer(pk);
                    }
                    else
                    {
                        throw new SocketException();
                    }
                }
                catch (SocketException sEx)
                {
                    Server.LOG("<Master Server> disconnected | <Reason = Connection lost>", E_LogCategory.Error);
                    HandleMasterServerDisconnected();
                    break;
                }
            }
        }

        public void DataProcessorForMasterServer(Packet packet)
        {
            switch (packet.action)
            {
                case 0:
                    FromMS_SendCurrentState(packet);
                    break;
                case 1:
                    FromMS_PlayThisRoom(packet);
                    break;
                default:
                    break;
            }
        }

        // Following functions will handle all RESPONSES from Master Server
        // **************************************************************************************************** //

        public void FromMS_SendCurrentState(Packet packet)
        {
            ToMS_SendCurrentState();
        }

        public void FromMS_PlayThisRoom(Packet packet)
        {
            MS_Action_1 response = new MS_Action_1();
            MS_Action_1.Convert(packet, out response);

            // Create new room
            RoomObject room = new RoomObject(response.roomID);

            Server.LOG("<RoomID = " + room.roomID + "> is assigned", E_LogCategory.Warning);

            // Send too dsConller to process
            server.dsController.PlayThisRoom(room);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        // Following functions will handle all REQUESTS from Master Server
        // **************************************************************************************************** //

        public void ToMS_SendCurrentState()
        {
            string jsonStr = "{\"secret\": \"" + server.serverConfig.masterServerSecretKey + "\"}";

            Packet packet = new Packet((int)E_DSCToMasterServerActions.HereIsMyCurrentState, jsonStr);
            packet.Send(masterServerSocket, false);
        }

        public void ToMS_DedicatedServerIsReadyToPlay(DSConnection dsConnection)
        {
            string jsonStr = "{\"IP\": \"" + dsConnection.dsIP + "\", \"port\": " + dsConnection.dsPort + ", \"roomID\": \""+ dsConnection.currentRoom.roomID+ "\"}";

            Packet packet = new Packet((int)E_DSCToMasterServerActions.DedicatedServerIsReadyToPlay, jsonStr);
            packet.Send(masterServerSocket, false);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

    }
}
