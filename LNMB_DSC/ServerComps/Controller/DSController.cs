﻿using ServerComps.DedicatedServer;
using ServerComps.MappingObject.DedicatedServer;
using Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ServerComps.Model;
using System.Diagnostics;

namespace ServerComps.Controller
{
    public enum E_DSCToDSActions
    {
        TellMeYourState = 0,
        TellMeYourStateResponse = 1,
        PlayThisRoom = 2
    }

    class DSController
    {
        public Server server;
        public DSListener dsListener;

        // Accepted connections
        public List<DSConnection> acceptedConnections;

        // Connection will be added to here first. After checking, add acceptedConnections
        public List<DSConnection> checkingConnections;

        // rooms waiting for available dedicated server
        public Dictionary<string, RoomObject> waitingRooms;

        // playing rooms
        public Dictionary<int, DSConnection> playingRooms;

        public DSController(Server inServer)
        {
            server = inServer;
            acceptedConnections = new List<DSConnection>();
            checkingConnections = new List<DSConnection>();
            waitingRooms = new Dictionary<string, RoomObject>();
            playingRooms = new Dictionary<int, DSConnection>();
        }

        public void Start()
        {
            dsListener = new DSListener(server);
        }

        public void AddNewDSConnection(DSConnection connection)
        {
            // First add to checking Connections
            checkingConnections.Add(connection);
            ToDS_SendMeYourCurrentState(connection);
        }

        public void RemoveDSConnection(DSConnection dsConnection)
        {
            if (dsConnection.bIsAccepted)
            {
                acceptedConnections.Remove(dsConnection);
                playingRooms.Remove(dsConnection.dsPort);
            }
            else
            {
                checkingConnections.Remove(dsConnection);
            }
        }

        // Handle responses from Dedicated Server ----------------------------------------------------------------------------------------------------

        public void ReceiveDataFromDedicatedServer(object dsConnection)
        {
            DSConnection dCon = (DSConnection)dsConnection;

            byte[] Buffer;
            int readBytes;

            while (dCon.bWantStopReceivingData == false)
            {
                try
                {
                    Buffer = new byte[dCon.socket.SendBufferSize];
                    readBytes = dCon.socket.Receive(Buffer);
                    Array.Resize(ref Buffer, readBytes);

                    if (readBytes > 0)
                    {
                        Packet pk = new Packet(Buffer);
                        DataProcessorForDedicatedServer(pk, dCon);
                    }
                    else
                    {
                        throw new SocketException();
                    }
                }
                catch (SocketException sEx)
                {
                    Server.LOG("<Dedicated Server> disconnected | <Reason = Connection lost>", E_LogCategory.Error);
                    Disconnect(dCon);
                    break;
                }
            }
        }

        public void Disconnect(DSConnection dsConnection)
        {
            // First remove from all lists
            RemoveDSConnection(dsConnection);

            // Then disconnect
            dsConnection.HandleDisconnected();
        }

        public void DataProcessorForDedicatedServer(Packet packet, DSConnection dsConnection)
        {
            switch (packet.action)
            {
                case 0:
                    FromDS_ReceiveDSState(packet, dsConnection);
                    break;
                case 1:
                    FromDS_ThidDSIsAvailable(dsConnection);
                    break;
                case 2:
                    FromDS_ThidDSIsReadyForPlayerToJoin(dsConnection);
                    break;
                default:
                    break;
            }
        }

        // Following functions will handle all RESPONSES from Dedicated Servers
        // **************************************************************************************************** //

        public void FromDS_ReceiveDSState(Packet packet, DSConnection dedicatedServerConnection)
        {
            DS_Action_0 response = new DS_Action_0();
            DS_Action_0.Convert(packet, out response);

            bool bSuccessful = false;
            string message;

            if (response.secretKey.Equals(server.serverConfig.dsSecretKey))
            {
                message = "Accept connection";
                bSuccessful = true;

                dedicatedServerConnection.SetAccepted(true);
                dedicatedServerConnection.dsPort = response.listeningPort;

                acceptedConnections.Add(dedicatedServerConnection);

                Server.LOG("Accept connection", E_LogCategory.Successful);
            }
            else
            {
                message = "Wrong secret key";
                Server.LOG("Wrong secret key", E_LogCategory.Error);
            }

            ToDS_SendMeYourCurrentStateReponse(bSuccessful, message, dedicatedServerConnection);

            if (bSuccessful == false)
            {
                Disconnect(dedicatedServerConnection);
            }
        }

        public void FromDS_ThidDSIsAvailable(DSConnection dsConnection)
        {
            Server.LOG("<DedicatedServer | Port = " + dsConnection.dsPort + "> is available", E_LogCategory.Warning);
            AssignRoomToDS(dsConnection);
        }

        public void FromDS_ThidDSIsReadyForPlayerToJoin(DSConnection dsConnection)
        {
            dsConnection.currentRoom.SetRoomState(E_RoomState.ReadyForPlayersToJoin);

            server.msController.ToMS_DedicatedServerIsReadyToPlay(dsConnection);
            Server.LOG("<RoomID = " + dsConnection.currentRoom.roomID + "> is ready to play", E_LogCategory.Warning);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        // Following functions will handle all REQUESTS to Dedicated Servers
        // **************************************************************************************************** //

        public void ToDS_SendMeYourCurrentState(DSConnection connection)
        {
            // Tell me your state and (including secret key)
            Packet packet = new Packet((int)E_DSCToDSActions.TellMeYourState, "");
            packet.Send(connection.socket, true);
        }

        public void ToDS_SendMeYourCurrentStateReponse(bool bSuccessful, string message, DSConnection dsConnection)
        {
            // Response
            string jsonString = "{\"response\":" + bSuccessful + ", \"message\":\"" + message + "\"}";

            Packet packet = new Packet((int)E_DSCToDSActions.TellMeYourStateResponse, jsonString);
            packet.Send(dsConnection.socket, true);
        }

        public void ToDS_PlayThisRoom(DSConnection dsConnection)
        {
            string jsonString = "{\"roomID\":\"" + dsConnection.currentRoom.roomID + "\"}";

            Packet packet = new Packet((int)E_DSCToDSActions.PlayThisRoom, jsonString);
            packet.Send(dsConnection.socket, true);
        }

        // **************************************************************************************************** //
        // End ----------------------------------------------------------------------------------------------------

        public DSConnection FindAvailableDS()
        {
            foreach (DSConnection con in acceptedConnections)
            {
                if (con.AreYouAvailable())
                {
                    return con;
                }
            }

            return null;
        }

        public void AssignRoomToDS(DSConnection dsConnection)
        {
            if (waitingRooms.Count() > 0)
            {
                RoomObject room = waitingRooms.First().Value;
                PlayThisRoom(room);
            }
        }

        public void PlayThisRoom(RoomObject room)
        {
            // Find available dedicated server
            DSConnection dsConnection = FindAvailableDS();

            if (dsConnection != null)
            {
                // Set new room
                dsConnection.SetCurrentRoom(room);

                // Add to playing rooms
                playingRooms.Add(dsConnection.dsPort, dsConnection);

                // Remove from waiting rooms
                waitingRooms.Remove(room.roomID);

                // Send
                ToDS_PlayThisRoom(dsConnection);

                Server.LOG("<DedicatedServer | Port = " + dsConnection.dsPort + "> will play <RoomID = " + room.roomID + ">", E_LogCategory.Warning);
            }
            else
            {
                // Create new dedicated server instance
                Process process = Process.Start(@server.serverConfig.dedicatedServerPath);
                waitingRooms.Add(room.roomID, room);
            }
        }        
    }
}
