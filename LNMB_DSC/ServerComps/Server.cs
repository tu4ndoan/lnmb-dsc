﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using Utilities;
using ServerComps.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ServerComps.Controller;

namespace ServerComps
{
    public struct ConnectionInfo
    {
        public IPEndPoint IP_EndPoint;

        public ConnectionInfo(string IP, int port)
        {
            IP_EndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
        }
    }

    public enum E_LogCategory
    {
        Log, Warning, Error, Successful
    }

    // ----------------------------------------------------------------------------------------------------

    class Server
    {
        public ServerConfig serverConfig;
        
        public DSController dsController;
        public MasterServerController msController;

        // ----------------------------------------------------------------------------------------------------

        // List of rooms which are assigned from Master Server but still waiting for available Dedicated Server
        public List<RoomObject> waitingRooms;

        // ----------------------------------------------------------------------------------------------------

        public Server(ServerConfig inServerConfig)
        {
            serverConfig = inServerConfig;

            // --------------------------------------------------

            waitingRooms = new List<RoomObject>();

            // --------------------------------------------------

            dsController = new DSController(this);
            msController = new MasterServerController(this);
        }

        public void Start()
        {
            LOG("Dedicated Server Controller started", E_LogCategory.Successful);

            dsController.Start();
            msController.Start();
        } 

        public static void LOG(string message, E_LogCategory logCategory)
        {
            StackFrame callStack = new StackFrame(1, true);
            string Time = DateTime.Now.ToString();

            switch (logCategory)
            {
                case E_LogCategory.Log:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case E_LogCategory.Warning:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                case E_LogCategory.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case E_LogCategory.Successful:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }

            Console.WriteLine(Time + " - " + callStack.GetFileName().Split('\\').Last() + " (" + callStack.GetFileLineNumber() + "): " + message);

            Console.ResetColor();
        }
    }
}
