﻿using ServerComps.DedicatedServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerComps.Model
{
    public enum E_RoomState
    {
        WaitingForAvailableDedicatedServer,
        WaitingForDedicatedServerLoadingMap,
        ReadyForPlayersToJoin,
        Playing
    }

class RoomObject
    {
        public string roomID;
        public E_RoomState roomState;

        public RoomObject(string inRoomID)
        {
            roomState = E_RoomState.WaitingForAvailableDedicatedServer;
            roomID = inRoomID;
        }

        public void SetRoomState(E_RoomState inRoomState)
        {
            roomState = inRoomState;
        }
    }
}
