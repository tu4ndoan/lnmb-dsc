﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    [DataContract]
    class ServerConfig
    {
        [DataMember(Name = "thisServerIP")]
        public string thisServerIP;

        [DataMember(Name = "listenDSPort")]
        public int listenDSPort;

        [DataMember(Name = "dsSecretKey")]
        public string dsSecretKey;

        [DataMember(Name = "masterServerIP")]
        public string masterServerIP;

        [DataMember(Name = "masterServerPort")]
        public int masterServerPort;

        [DataMember(Name = "masterServerSecretKey")]
        public string masterServerSecretKey;

        [DataMember(Name = "dedicatedServerPath")]
        public string dedicatedServerPath;

        public ServerConfig()
        {

        }

        public static void Convert(string data, out ServerConfig targetInstance)
        {
            targetInstance = new ServerConfig();
            MemoryStream ms = new MemoryStream(Encoding.ASCII.GetBytes(data));
            DataContractJsonSerializer jsonData = new DataContractJsonSerializer(targetInstance.GetType());
            targetInstance = jsonData.ReadObject(ms) as ServerConfig;
        }
    }
}
