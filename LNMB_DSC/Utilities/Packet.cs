﻿using ServerComps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    [DataContract]
    class Packet
    {
        [DataMember(Name = "act")]
        public int action { get; set; } // type of action

        [DataMember(Name = "dat")]
        public string data { get; set; } // data

        public Packet()
        {

        }

        public Packet(int inAction, string inData)
        {
            this.action = inAction;
            this.data = inData;
        }

        public bool Send(Socket socket, bool bNeedDelimiter)
        {
            try
            {
                int bytes = 0;
                string str = DataContractJsonSerializerHelper.GetJson(this);

                if (bNeedDelimiter)
                {
                    str = str + "//";
                }

                bytes = socket.Send(Encoding.ASCII.GetBytes(str));

                if (bytes > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SocketException sEx)
            {
                Server.LOG(sEx.ToString(), E_LogCategory.Error);
                return false;
            }
        }

        public Packet(byte[] bytes)
        {
            try
            {
                Packet pk = new Packet();
                MemoryStream ms = new MemoryStream(bytes);
                DataContractJsonSerializer jsonData = new DataContractJsonSerializer(pk.GetType());
                pk = jsonData.ReadObject(ms) as Packet;

                //Convert
                action = pk.action;
                data = pk.data;
            }
            catch (Exception ex)
            {
                Server.LOG("Packet is invalid", E_LogCategory.Error);
            }
        }
    }
}
