﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Interfaces
{
    interface IJsonObject
    {
        string GetJsonString<T>(T Object);
    }
}
